Name: pistache
Version: 73f248acd6db4c53e6604577b7e13fd5e756f96f
Release: 1%{?dist}
Summary: A high-performance REST Toolkit written in C++
License: MIT
URL: http://pistache.io/
Source0: https://github.com/oktal/pistache/archive/%{version}.zip

BuildRequires: cmake
BuildRequires: make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: rapidjson-devel
BuildRequires: openssl-devel

Requires: rapidjson
Requires: openssl

%description
A high-performance REST Toolkit written in C++

%package devel
Summary:  %{summary}
Provides: %{name} = %{version}-%{release}

%description devel
%{description}

%prep
%autosetup -p1 -n %{name}-%{version}

rm -rf build
mkdir build

%build
pushd build

%cmake ../ \
	-DCMAKE_BUILD_TYPE=Release \
	-DPISTACHE_BUILD_EXAMPLES=true \
	-DPISTACHE_BUILD_TESTS=false \
	-DPISTACHE_BUILD_DOCS=false \
	-DPISTACHE_USE_SSL=true

%make_build

popd

%install
pushd build
%make_install
popd

%files devel
%{_libdir}/libpistache.so.0.0.002-git20200301
%{_libdir}/libpistache.so.0
%{_libdir}/libpistache.so
%{_libdir}/libpistache.a
%{_libdir}/cmake/pistache/PistacheTargets.cmake
%{_libdir}/cmake/pistache/PistacheTargets-release.cmake
%{_libdir}/cmake/pistache/PistacheConfig.cmake
%{_libdir}/pkgconfig/*
%{_includedir}/%{name}

%changelog
* Sat Apr 18 2020 Erico Nunes <nunes.erico@gmail.com> 73f248acd6db4c53e6604577b7e13fd5e756f96f
- Initial build.
